# Showcase: Augmented Reality Insurance Card

## Introduction

This showcase uses an Image Target to identify an insurance card (yes, it won't work on other cards 💳) and projects 2D/3D data on it. To keep it simple, we reduced it to:

* Full Name
* Customer Number
* Social Insurance Number (in Switzerland we call it AHV number)
* Settled and Remaining Deductible
* Settled and Remaining Copayment

For more detailed information, check out our Medium post: "Showcase: Augmented Reality Insurance Card" at https://medium.com/47-north-labs

## Prerequisites

* Webcam
* Vuforia 7.5.20
* Unity (version used in this project: 2018.2.12f1)

## Getting started

1. Pull the code

`git clone https://gitlab.com/47northlabs/47northlabs/augmented-reality-insurance-card.git`

2. Open Unity and select **open dialog**.

![Unity Dialog](unity-open-1.png)

3. Navigate to checked out codebase, **select the folder** and click on **open**.

![Open Dialog](unity-open-2.png)

4. After the project is loaded, use **project** window, to select **Assets** and double click **Insurance Card** (Scene).

![Scene selection](unity-open-3.png)

5. Click on **play** (`⌘-P`) to compile and run project. Hold a **swiss insurance card (or printout)** into webcam.

![Unity Preview](unity-preview.png)

6. (optional) Go to File -> Build Settings, select desired **platform** (android or ios) and click on **build**.

## Demovideo

[![Augmented Reality Insurance Card](http://img.youtube.com/vi/GepKxt16nSk/0.jpg)](http://www.youtube.com/watch?v=GepKxt16nSk "Augmented Reality Insurance Card")

## Need help? More information?

* Story "Showcase: Augmented Reality Insurance Card" at https://medium.com/47-north-labs
* 47N Homepage: https://www.47northlabs.com/
* Author: Jeremy Haas, jeremy.haas@47northlabs.com
